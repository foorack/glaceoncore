echo $1
export GOPATH=$1
go get gopkg.in/mgo.v2
go get gopkg.in/mgo.v2/bson
go get gopkg.in/igm/sockjs-go.v2/sockjs
go get github.com/kisom/aescrypt
go get github.com/bitmark-inc/go-argon2
cd $GOPATH/src/github.com/bitmark-inc/go-argon2
git clone https://github.com/P-H-C/phc-winner-argon2.git
cd phc-winner-argon2
sudo make
make test
sudo cp include/argon2.h /usr/local/include/
sudo cp libargon2.a /usr/local/lib/
go test github.com/bitmark-inc/go-argon2
echo .
echo Done