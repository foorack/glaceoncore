package minecraftapi

import (
	"encoding/json"
	"net/http"
)

// Result represents the JSON result from MC-API.NET name api
type Result struct {
	UUIDShort    string `json:"uuid"`
	UUID          string `json:"uuid_formatted"`
	Name        string `json:"name"`
}

// GetResultByUUID queries the mc-api.net to fetch data by uuid
func GetResultByUUID(uuid string, target interface{}) error {
	r, err := http.Get("https://mcapi.ca/name/uuid/" + uuid)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

// GetResultByUsername queries the mc-api.net to fetch data by name
func GetResultByUsername(username string, target interface{}) error {
	r, err := http.Get("https://mcapi.ca/uuid/player/" + username)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}