package glaceonmodels

import "gopkg.in/mgo.v2/bson"

type (
	// Code represents a code entry used to create a new user
	Code struct {
		ID   bson.ObjectId `json:"id" bson:"_id"`
		UUID string        `json:"uuid" bson:"uuid"`
		Code int           `json:"code" bson:"code"`
	}
	
	// User represents a user
	User struct {
		ID   bson.ObjectId `json:"id" bson:"_id"`
		UUID string        `json:"uuid" bson:"uuid"`
		Password string    `json:"password" bson:"password"`
	}
	
	// Board represents a list of grouped categories
	Board struct {
		ID   bson.ObjectId `json:"id" bson:"_id"`
		Title string        `json:"title" bson:"title"`
		Order int    `json:"order" bson:"order"`
	}
	
	// Category represents a list of grouped topics
	Category struct {
		ID   bson.ObjectId `json:"id" bson:"_id"`
		Title string        `json:"title" bson:"title"`
		Order int    `json:"order" bson:"order"`
		Board bson.ObjectId    `json:"board" bson:"board"`
	}
	
	// Topic represents a list of grouped posts
	Topic struct {
		ID   bson.ObjectId `json:"id" bson:"_id"`
		Title string        `json:"title" bson:"title"`
		Category bson.ObjectId    `json:"category" bson:"category"`
		UUID string    `json:"creator" bson:"creator"`
		Timestamp string    `json:"timestamp" bson:"timestamp"`
		
	}
	
	// Post represents a user text entry in a topic
	Post struct {
		ID   bson.ObjectId `json:"id" bson:"_id"`
		Topic bson.ObjectId    `json:"topic" bson:"topic"`
		UUID string    `json:"creator" bson:"creator"`
		Timestamp string    `json:"timestamp" bson:"timestamp"`
		Text string    `json:"text" bson:"text"`
	}
	
)
