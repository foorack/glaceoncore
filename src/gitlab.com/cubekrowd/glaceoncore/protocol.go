package main

import (
	"encoding/json"
	"encoding/base64"
	"strconv"
	"strings"
	"time"

	"github.com/bitmark-inc/go-argon2"
	"gitlab.com/cubekrowd/glaceonmodels"
	"gitlab.com/cubekrowd/minecraftapi"
	"gopkg.in/mgo.v2/bson"
	
	"github.com/kisom/aescrypt/strongbox"
)

// EmptyPayload is a generic Payload implementation to represent empty payload.
type EmptyPayload struct {
}

// InPayloadContent is the Payload implementation of the inbound packet "content".
type InPayloadContent struct {
	URL  string `json:"url"`
	Hash string `json:"hash"`
}

// OutPayloadContent is the Payload implementation of the outbound packet "content".
type OutPayloadContent struct {
	URL string `json:"url"`
}

// InPayloadRegister is the Payload implementation of the inbound packet "register".
type InPayloadRegister struct {
	Code     int    `json:"code"`
	Password string `json:"password"`
}

// OutPayloadRegister is the Payload implementation of the outbound packet "register".
type OutPayloadRegister struct {
	UUID     string `json:"uuid"`
	Username string `json:"username"`
	Token    string `json:"token"`
}

// InPayloadLogin is the Payload implementation of the inbound packet "login".
type InPayloadLogin struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// OutPayloadLogin is the Payload implementation of the outbound packet "login".
type OutPayloadLogin struct {
	UUID     string `json:"uuid"`
	Username string `json:"username"`
	Token    string `json:"token"`
}

func handlerContent(packet InPacket) (OutPacket, error) {
	var header PacketHeader
	var outpayload Payload
	var inpayload InPayloadContent
	err := json.Unmarshal(*packet.Payload, &inpayload)
	if err != nil {
		return OutPacket{}, err
	}
	
	// default header, only change if neccesary
	header = PacketHeader{Action: "content", Status: 200}
			
	if strings.Compare(inpayload.Hash, "#/") == 0 {
		outpayload = OutPayloadContent{URL: "ajax/index.html"}
	} else if strings.Compare(inpayload.Hash, "#/rules") == 0 {
		outpayload = OutPayloadContent{URL: "ajax/rules.html"}
	} else if strings.HasPrefix(inpayload.Hash, "#/games") {
		outpayload = OutPayloadContent{URL: "ajax/games.html"}
	} else if strings.Compare(inpayload.Hash, "#/forum") == 0 {
		outpayload = OutPayloadContent{URL: "ajax/forum.html"}
	} else if strings.Compare(inpayload.Hash, "#/staff") == 0 {
		outpayload = OutPayloadContent{URL: "ajax/staff.html"}
	} else if strings.Compare(inpayload.Hash, "#/bans") == 0 {
		outpayload = OutPayloadContent{URL: "ajax/bans.html"}
	} else {
		header = PacketHeader{Action: "content", Status: 404}
		outpayload = OutPayloadContent{URL: "ajax/404.html"}
	}
	
	response := OutPacket{Header: header, Payload: outpayload}
	return response, nil
}

func handlerRegister(packet InPacket) (OutPacket, error) {
	var header PacketHeader
	var outpayload Payload
	var inpayload InPayloadRegister
	err := json.Unmarshal(*packet.Payload, &inpayload)
	if err != nil {
		return OutPacket{}, err
	}

	// Check if the code is in the code database
	var codes []glaceonmodels.Code
	err = session.DB("glaceon").C("codes").Find(bson.M{"code": inpayload.Code}).All(&codes)
	if err != nil {
		panic(err)
	}

	switch len(codes) {
	case 0: // code not found
		header = PacketHeader{Action: "register", Status: 404}
		outpayload = EmptyPayload{}
	case 1: // found
		var code = codes[0]
		// first off, remove the code from the code list
		err = session.DB("glaceon").C("codes").Remove(bson.M{"code": code.Code})
		if err != nil {
			panic(err)
		}

		// second, hash the password with argon2
		hash, err := argon2.HashEncoded(ctx, []byte(inpayload.Password), []byte("glaceon" + strings.ToLower(code.UUID)))
		if err != nil {
			panic(err)
		}

        // third, generate the authkey
        key, _ := base64.StdEncoding.DecodeString(config.SignKey)
        authtoken, _ := strongbox.Seal([]byte("1#" + code.UUID + "#" + time.Now().String()), key)

        // fourth, insert user into database
        err = session.DB("glaceon").C("users").Insert(glaceonmodels.User{ID: bson.NewObjectId(), UUID: code.UUID, Password: hash})
		if err != nil {
			panic(err)
		}
		
		// fifth, find the UUID.
    	var result []minecraftapi.Result
    	err = minecraftapi.GetResultByUUID(code.UUID, &result)
	    if err != nil {
            panic(err)
        }

        // sixth and finally, send back the packet with auth token
		header = PacketHeader{Action: "register", Status: 200}
		outpayload = OutPayloadRegister{UUID: code.UUID, Username: result[0].Name, Token: base64.StdEncoding.EncodeToString(authtoken)}
	default: // two or more
		panic("Multiple entries found for code: " + strconv.Itoa(inpayload.Code))
	}

	response := OutPacket{Header: header, Payload: outpayload}
	return response, nil
}


func handlerLogin(packet InPacket) (OutPacket, error) {
	var header PacketHeader
	var outpayload Payload
	var inpayload InPayloadLogin
	err := json.Unmarshal(*packet.Payload, &inpayload)
	if err != nil {
		return OutPacket{}, err
	}

    // first, find the UUID.
	var result []minecraftapi.Result
	err = minecraftapi.GetResultByUsername(inpayload.Username, &result)
	if err != nil {
		panic(err)
	}

    // second, check if the user is in the database.
	var users []glaceonmodels.User
	err = session.DB("glaceon").C("users").Find(bson.M{"uuid": result[0].UUID}).All(&users)
	if err != nil {
		panic(err)
	}
	
	if len(users) != 1 {
	    header = PacketHeader{Action: "login", Status: 401}
		outpayload = EmptyPayload{}
	} else {
	    
	    // third, verify the hash
	    ok, err := argon2.VerifyEncoded(users[0].Password, []byte(inpayload.Password))
		if err != nil {
			panic(err)
		}
		if !ok {
		    header = PacketHeader{Action: "login", Status: 401}
		    outpayload = EmptyPayload{}
		} else {
		    // password OK, fourth, generate authkey
            key, _ := base64.StdEncoding.DecodeString(config.SignKey)
            authtoken, _ := strongbox.Seal([]byte("1#" + users[0].UUID + "#" + time.Now().String()), key)
            
            // fifth and finally, send back the packet with auth token
		    header = PacketHeader{Action: "login", Status: 200}
		    outpayload = OutPayloadRegister{UUID: users[0].UUID, Username: result[0].Name, Token: base64.StdEncoding.EncodeToString(authtoken)}
		}
	    
	}
	
	response := OutPacket{Header: header, Payload: outpayload}
	return response, nil
}