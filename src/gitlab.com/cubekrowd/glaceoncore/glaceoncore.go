package main

import (
	"log"
	"net/http"
	"os"
	"strconv"
	"encoding/base64"

	"gitlab.com/cubekrowd/glaceonmodels"
	"github.com/bitmark-inc/go-argon2"

	"gopkg.in/igm/sockjs-go.v2/sockjs"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	
	"github.com/kisom/aescrypt/strongbox"
)

var config Config
var session *mgo.Session
var ctx = &argon2.Context{
    Iterations:  5,
    Memory:      1 << 16,
    Parallelism: 2,
    HashLen:     32,
    Mode:        argon2.ModeArgon2i,
}

func main() {
	log.Println("Starting Glaceon...")

	// Load configuration
	log.Println("Loading configuration...")
	var err error
	config, err = getConfig()
	if err != nil {
		// Error printing is handled by getConfig()
		os.Exit(1)
	}
	key, _ := base64.StdEncoding.DecodeString(config.SignKey)
	suitable := strongbox.KeyIsSuitable(key)
	if !suitable {
	    gka, _ := strongbox.GenerateKey()
	    log.Fatal("SignKey not suitable. New generated key: " + base64.StdEncoding.EncodeToString(gka))
	    os.Exit(1)
	}
	// DEBUG: Print configuration to console
	log.Println()
	log.Println("  address=" + config.Address)
	log.Println("  port=" + strconv.Itoa(config.Port))
	log.Println("  mongodburi=" + config.MongoDBURI)
	log.Println()
	log.Println("Configuration loaded.")

	// Setup mongodb connection
	log.Println("Connecting to MongoDB...")
	session = getSession()
	// Add extra code for DEBUG
	i, err := session.DB("glaceon").C("codes").Count()
	if err != nil {
		panic(err)
	}
	if i == 0 {
		err = session.DB("glaceon").C("codes").Insert(glaceonmodels.Code{ID: bson.NewObjectId(), Code: 5469, UUID: "7e1b2286-e794-46fd-ba8b-f806dc7062e1"})
		if err != nil {
			panic(err)
		}
	}
	log.Println("MongoDB connection successful.")

	// Start SockJS server and listen on /_socket
	log.Println("Starting SockJS server...")
	// Setup packet handler
	packetHandlerInit()
	// Create our SockJS listener with our socket packet handler
	handler := sockjs.NewHandler("/_socket", sockjs.DefaultOptions, socketHandler)
	// Start listen for incoming connections
	log.Fatal(http.ListenAndServe(config.Address+":"+strconv.Itoa(config.Port), handler))
	log.Println("SockJS server started...")
}

func getSession() *mgo.Session {
	// Connect to the mongodb server
	s, err := mgo.Dial(config.MongoDBURI)

	// Check for connection-error, if this happens double check mongo is running
	if err != nil {
		panic(err)
	}
	return s
}
