package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

// Config represents the JSON configuration file.
type Config struct {
	Address    string `json:"address"`
	Port       int    `json:"port"`
	MongoDBURI string `json:"mongodburi"`
	SignKey    string `json:"signkey"`
}

func getConfig() (Config, error) {

	var config Config

	if _, err := os.Stat("config.json"); os.IsNotExist(err) {
		log.Println("Could not find config.json, creating it now...")
		config = Config{Address: "", Port: 80, MongoDBURI: "mongodb://localhost"}

		j, jerr := json.MarshalIndent(config, "", "  ")
		if jerr != nil {
			log.Fatal("Error while indenting configuration: ", err)
			return config, err
		}

		err = ioutil.WriteFile("config.json", j, 0660)
		if err != nil {
			log.Fatal("Error while writing to config.json: ", err)
			return config, err
		}
	}

	// Read from configuration file
	file, err := ioutil.ReadFile("./config.json")
	if err != nil {
		// Fail if we fail
		log.Fatal("Error reading configfile: ", err)
		return config, err
	}

	// Parse the configuration file into JSON
	err = json.Unmarshal(file, &config)
	if err != nil {
		// Fail if we fail
		log.Fatal("Error parsing configfile: ", err)
		return config, err
	}

	return config, nil
}
