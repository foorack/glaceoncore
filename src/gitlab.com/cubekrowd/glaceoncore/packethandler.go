package main

import (
	"encoding/json"
	"log"

	"gopkg.in/igm/sockjs-go.v2/sockjs"
)

// InPacket represents the JSON packet the client sends.
type InPacket struct {
	Header  PacketHeader     `json:"header"`
	Payload *json.RawMessage `json:"payload"`
}

// OutPacket represents the JSON packet the server sends.
type OutPacket struct {
	Header  PacketHeader `json:"header"`
	Payload Payload      `json:"payload"`
}

// PacketHeader is the Packet header.
type PacketHeader struct {
	Action string `json:"action"`
	Status int    `json:"status"`
}

// Payload is an interface represeting the outbound payload
type Payload interface {
}

var handlers map[string]func(InPacket) (OutPacket, error)

func packetHandlerInit() {
	handlers = make(map[string]func(InPacket) (OutPacket, error))

	handlers["content"] = handlerContent
	handlers["register"] = handlerRegister
	handlers["login"] = handlerLogin
}

func socketHandler(session sockjs.Session) {
	for {
		if msg, err := session.Recv(); err == nil {

			// DEBUG Print it out
			log.Println(msg)

			// Decode the packet
			var inpacket InPacket

			// Generally decode the whole packet
			var packetmap map[string]*json.RawMessage
			err := json.Unmarshal([]byte(msg), &packetmap)
			if err != nil {
				session.Close(400, "Invalid packet.")
				return
			}

			// Decode the header but wait wih payload
			var packetHeader PacketHeader
			err = json.Unmarshal(*packetmap["header"], &packetHeader)
			if err != nil {
				session.Close(400, "Invalid header.")
				return
			}
			inpacket = InPacket{Header: packetHeader, Payload: packetmap["payload"]}

			// Lookup if there is a packet handler
			if _, ok := handlers[inpacket.Header.Action]; !ok {
				session.Close(501, "Uknown packet action.")
				return
			}

			// Execute packet handler and get response
			response, err := handlers[inpacket.Header.Action](inpacket)
			// There was an error while processing the packet.
			if err != nil {
				log.Fatal("Error while processing packet: ", err)
				session.Close(500, "Error while processing packet.")
				return
			}
			// Encode and return. This should logically not cause an error.
			p, err := json.Marshal(response)
			if err != nil {
				log.Fatal("Error parsing packet: ", err)
				session.Close(400, "Error parsing packet.")
				return
			}
			session.Send(string(p))

			continue
		}
		break
	}
}
